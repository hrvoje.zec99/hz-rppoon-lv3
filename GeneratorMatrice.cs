using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV3
{
    class GeneratorMatrice
    {
        //2.zadatak
        private static GeneratorMatrice instance;
        private RandomGenerator randomGenerator;

        private GeneratorMatrice()
        {
            this.randomGenerator = RandomGenerator.GetInstance();
        }
        public static GeneratorMatrice GetInstance()
        {
            if (instance == null)
            {
                instance = new GeneratorMatrice();
            }
            return instance;
        }

        public double[][] createMatrix(int a, int b)
        {
            double[][] matrix = new double[a][];
            for(int i = 0; i < a; i++)
            {
                matrix[i] = new double[b];
                for(int j = 0; j < b; j++)
                {
                    matrix[i][j] = randomGenerator.NextDouble();
                    Console.Write(matrix[i][j]);
                }
                Console.WriteLine();
            }
            return matrix;
        }
    }
}