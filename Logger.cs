using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV3
{
    class Logger
    {
        //3.zadatak
        private static Logger instance;
        string filePath;
        private Logger()
        {
            this.filePath = @"C:\Users\hrco\Desktop\zadatak1.txt";

        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(message);
            }

        }
        public void setFilePath(string filePath)
        {
            this.filePath = filePath;
        }
    }
}