using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.zadatak
            string filePath = @"C:\Users\hrco\Desktop\zadatak1.txt";
            Dataset dataset = new Dataset(filePath);
            Dataset newDataset = (Dataset)dataset.Clone();
            //2.zadatak
            GeneratorMatrice generatorMatrice = GeneratorMatrice.GetInstance();
            double[][] matrix = new double[3][];
            for(int i = 0; i < 3; i++)
            {
                matrix[i] = new double[3];
            }
            matrix = generatorMatrice.createMatrix(3, 3);
            //3.zadatak
            Logger logger = Logger.GetInstance();
            logger.log("Hrvoje");
            //4.zadatak
            ConsoleNotification consoleNotification = new ConsoleNotification("Hrvoje", "Labos", "RPPOON", DateTime.Now, Category.ALERT, ConsoleColor.Blue);
            NotificationManager notificationManager = new NotificationManager();

            notificationManager.Display(consoleNotification);
        }
    }
}
